﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Odev.Models
{
    public class UyeGirisi
    {
        [Required]
        [Display(Name ="Kullanıcı Adı")]
        [StringLength(20,ErrorMessage ="{0} en az {2} karakter olmalıdır",MinimumLength =4)]
        public string GirisAdSoyad { get; set; }


        [Required]
        [Display(Name = "Şifre")]
        [StringLength(20, ErrorMessage = "{0} en az {2} karakter olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public int GirisSifre { get; set; }
    }
}