﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Odev.Models
{
    public class UyeOl
    {      
        public string AdSoyad { get; set; }
        public string Sifre { get; set; }
        public string Meslek { get; set; }
        public string EPosta { get; set; }
        public string TelNo { get; set; }
    }
}