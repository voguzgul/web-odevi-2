﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;
using System.Web.Security;
using System.Globalization;
using System.Resources;

namespace Odev.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult UyeOl(string adsoyad, string sifre, string meslek, string eposta, string telefon)
        {
            if (HttpRuntime.Cache["yeniuyelistesi"] == null)//öğrenci listesinde kimse yok ise
            {
                List<UyeOl> uyelistesi = new List<UyeOl>();
                UyeOl yeniuye = new UyeOl();
                yeniuye.AdSoyad = adsoyad;
                yeniuye.Sifre = sifre;
                yeniuye.Meslek = meslek;
                yeniuye.EPosta = eposta;
                yeniuye.TelNo = telefon;
                uyelistesi.Add(yeniuye);
                HttpRuntime.Cache["yeniuyelistesi"] = uyelistesi;//ramda yeniuyelistesi adında yer.açar boxing işlemide denilir.
            }
            else//eğer listede br değer varsa mevcut listedeki veriyi listeye atar
            {
                List<UyeOl> uyelistesi = (List<UyeOl>)HttpRuntime.Cache["yeniuyelistesi"];
                UyeOl yeniuye = new Models.UyeOl();
                yeniuye.AdSoyad = adsoyad;
                yeniuye.Sifre = sifre;
                yeniuye.Meslek = meslek;
                yeniuye.EPosta = eposta;
                yeniuye.TelNo = telefon;
                uyelistesi.Add(yeniuye);
                HttpRuntime.Cache["yeniuyelistesi"] = uyelistesi;
            }
            return RedirectToAction("yeniListe");//bu action başka bi action yonlendirme           
        }      
        
        public ActionResult UyeGirisi()
        {
            return View();
        }
        public ActionResult yeniListe()
        {
            var model = (List<UyeOl>)HttpRuntime.Cache["yeniuyelistesi"];// kutuyu açıyoruz. yukarıdaki kutuyu açma işlemine unboxing denir.
            return View(model);
        }
    }
}